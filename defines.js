export const DEFINES = {
  ALL_ARTICLE: 1,
  PUBLISHED_ARTICLE: 2,
  UNPUBLISHED_ARTICLE: 3,
  FUTURE_ARTICLE: 4,
  FILTER_ARTICLES: [
    { value: 1, text: "All" },
    { value: 2, text: "Published" },
    { value: 3, text: "Unpublished" },
    { value: 4, text: "Coming Soon" },
  ],
};
